package enums

enum ValidationType {
    NUMERIC("NUMERIC", "numeric"),
    EMAIL("EMAIL", "email"),
    REQUIRED("REQUIRED", "required"),
    MAX_CHAR_LENGTH("MAX_CHAR_LENGTH", "maxCharLength"),
    MIN_CHAR_LENGTH("MIN_CHAR_LENGTH", "minCharLength"),
    MAX_RANGE("MAX_RANGE", "maxRange"),
    MIN_RANGE("MIN_RANGE", "minRange")

    String name
    String value

    ValidationType(String name, String value) {
        this.name = name
        this.value = value
    }
}
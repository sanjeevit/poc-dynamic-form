package enums.poc

/**
 * Created by intelligrape on 8/9/17.
 */
class FormFieldDTO {
    List<Long> fieldIds
    Long formId


    @Override
    public String toString() {
        return "FormFieldDTO{" +
                "fieldIds=" + fieldIds +
                ", formId=" + formId +
                '}';
    }
}

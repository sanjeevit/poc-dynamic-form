package enums

enum DataType {
    TEXT("TEXT", "text"),
    DATE("DATE", "date"),
    NUMBER("NUMBER", "number"),
    RADIO_BUTTON("RADIO_BUTTON", "RadioButton")

    String name
    String value

    DataType(String name, String value) {
        this.name = name
        this.value = value
    }
}
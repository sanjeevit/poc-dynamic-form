function abc() {
    // alert("abc")
}

function validate(currentElement) {
    var $nextTD = $(currentElement).closest("td").next();
    var currentValue = $(currentElement).val();
    var fieldId = $(currentElement).data("field");

    $.post("/validation/validate",
        {
            id: fieldId,
            value: currentValue
        },
        function (data, status) {
            $nextTD.html(data);
        });
}
/*
 function loadDoc() {
 $.post("/validate",
 {
 id: "1",
 value: "10"
 },
 function(data, status){
 alert("Data: " + data + "\nStatus: " + status);
 document.getElementById("validationDiv").innerHTML = this.responseText;
 });



 /!*
 var xhttp = new XMLHttpRequest();
 xhttp.onreadystatechange = function () {
 if (this.status === 200) {
 document.getElementById("validationDiv").innerHTML = this.responseText;
 }
 };
 xhttp.open("GET", "ajax_info.txt", true);
 xhttp.send();*!/
 }*/

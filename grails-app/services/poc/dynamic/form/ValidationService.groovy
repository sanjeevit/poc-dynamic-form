package poc.dynamic.form

import enums.ValidationType
import grails.gorm.transactions.Transactional
import org.apache.commons.lang.math.NumberUtils

@Transactional
class ValidationService {

    static final String EMAIL_PATTERN = /[a-z0-9_-]*([.][a-z0-9_-]+)*([+][0-9]+)*[@][a-z0-9-]*[a-z0-9.]+[a-z0-9]*/

    boolean verifyValidation(String value, Validation validation) {
        println "validation.validationType" + validation.validationType
        boolean validatonStatus = false;
        switch (validation.validationType) {
            case ValidationType.EMAIL:
                validatonStatus = validateEmail(value)
                break;
            case ValidationType.NUMERIC:
                validatonStatus = validateNumeric(value)
                break;
            case ValidationType.REQUIRED:
                validatonStatus = validateRequired(value)
                break;
            case ValidationType.MAX_CHAR_LENGTH:
                validatonStatus = validateMaxCharLength(value, validation)
                break;
            case ValidationType.MIN_CHAR_LENGTH:
                validatonStatus = validateMinCharLenth(value, validation)
                break;
            case ValidationType.MAX_RANGE:
                validatonStatus = validateMaxRange(value, validation)
                break;
            case ValidationType.MIN_RANGE:
                validatonStatus = validateMinRange(value, validation)
                break;
        }
        return validatonStatus
    }

    boolean validateEmail(String email) {
        return email.toLowerCase().matches(EMAIL_PATTERN)
    }

    boolean validateNumeric(String numericValue) {
        return NumberUtils.isNumber(numericValue)
    }

    boolean validateRequired(String value) {
        if (value)
            return true
        else
            return false
    }

    boolean validateMaxCharLength(String value, Validation validation) {
        if (value && validation) {
            validation.value.length() >= value.length()
        } else
            return true

    }

    boolean validateMinCharLenth(String value, Validation validation) {
        if (value && validation) {
            validation.value.length() <= value.length()
        } else
            return true
    }

    boolean validateMaxRange(String value, Validation validation) {
        if (value && validation) {
            if (validateNumeric(value)) {
                Integer.parseInt(validation.value) >= Integer.parseInt(value)
            } else
                return false
        } else {
            return true
        }
    }

    boolean validateMinRange(String value, Validation validation) {
        if (value && validation) {
            if (validateNumeric(value)) {
                Integer.parseInt(validation.value) <= Integer.parseInt(value)
            } else
                return false
        } else
            return true
    }

}

<%@ page import="enums.DataType" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'mainForm.name', default: 'MainForm')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-formFields" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                 default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><g:link class="create" controller="mainForm" action="index">Form List</g:link></li>
    </ul>
</div>
<center><h1>${mainForm.title}</h1></center>
<g:form action="submitForm" controller="mainForm">
<table>
    <g:each in="${mainForm.fields}" var="field">
        <tr>
            <td>
                <label>${field.label}:</label>
            </td>
            <td>
                <g:if test="${field.dataType == DataType.RADIO_BUTTON}">
                    <g:radioGroup name="lovesGrails"
                                  labels="['Yes!','Of course!','Always!']"
                                  values="[1,2,3]">
                        <p>${it.label} ${it.radio}</p>
                    </g:radioGroup>

                    <g:radioGroup name="test"
                                  labels="${field.radioText}"
                                  values="${1..(field.radioText.size())}">
                        <p>${it.radio} ${it.label} </p>
                    </g:radioGroup>
                </g:if>
                <g:else>
                    <input data-field="${field.id}" type="${field.dataType}" name="${field.name}" value="${field.defaultValue}"
                           placeholder="${field.placeholder}" class="" onblur="validate(this)"/>
                    <p id="validationDiv"></p>
                </g:else>
            </td>
            <td></td>
        </tr>
    </g:each>
    <tr>
        <td><g:submitButton name="submit" value="submit" class="submit" onClick="abc()"/></td>
    </tr>
</table>
</g:form>
<asset:javascript src="customValidation.js"/>
</body>
</html>

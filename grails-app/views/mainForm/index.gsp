<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'mainForm.name', default: 'MainForm')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-mainForm" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                               default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        %{--<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.name"/></a></li>--}%
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-mainForm" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>

    <table>
        <th>id</th>
        <th>name</th>
        <th>add fields</th>
        <th>Preview Form</th>
        <g:each in="${list}" var="customform">

            <tr>
                <td>
                    ${customform.id}
                </td>
                <td>
                    ${customform.title}
                </td>
                <td>
                    <g:link controller="mainForm" action="addFields" params="${[id: customform.id]}">add fields</g:link>
                </td>
                <td>
                    <g:link controller="mainForm" action="preview" params="${[id: customform.id]}">preview</g:link>
                </td>
            </tr>
        </g:each>
    </table>
</div>
</body>
</html>
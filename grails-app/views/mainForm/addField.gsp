<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'mainForm.name', default: 'MainForm')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-mainForm" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                               default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><g:link class="list" controller="formFields" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="create" controller="formFields" action="create">New Field</g:link></li>
    </ul>
</div>
<center><h1>${title}</h1></center>

<h1>Already added fields</h1>
<table>
    <tr><th>id</th>
        <th>label</th>
        <th>dataType</th>
        <th>defaultValue</th>
        <th>placeholder</th>
        <th>validationClass</th>
        <th>remove</th>
    </tr>
    <g:if test="${selectedList.size()}">
        <g:each in="${selectedList}" value="field" var="field">
            <tr>
                <td>${field.id}</td>
                <td>${field.name}</td>
                <td>${field.dataType}</td>
                <td>${field.defaultValue}</td>
                <td>${field.placeholder}</td>
                <td>${field.validationClass}</td>
                <td><g:link controller="mainForm" action="removeField"
                            params="[formId: id, fieldId: field.id]">remove</g:link></td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr><td>NO Result Found</td></tr>
    </g:else>
</table>
<br>
<br>
<br>
<br>

<h1>Add new fields</h1>
<g:form action="saveFormFields" controller="mainForm" name="saveFormField" id="saveFormField">
    <g:select multiple="true" name="fieldIds" from="${list}" optionValue="name" optionKey="id" value="${label}"
              noSelection="['': '-Choose your age-']"/>
    <g:hiddenField name="formId" value="${id}"/>
    <g:submitButton name="submit" value="add"/>
</g:form>

</div>
</body>
</html>

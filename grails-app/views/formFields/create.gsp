<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'formFields.name', default: 'FormFields')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<a href="#create-formFields" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                   default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="create-formFields" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.formFields}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.formFields}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form controller="formFields" action="save"  method="POST">
        <table>
            <tr>
                <td><label>Name</label></td>
                <td><input type="text" name="name" value="${field?.title}"/></td>
            </tr>
            <tr>
                <td><label>Label</label></td>
                <td><input type="text" name="label" value="${field?.label}"/></td>
            </tr>
            <tr>
                <td><label>Data Type</label></td>
                <td><g:select name="dataType" value="${field?.dataType}" from="${enums.DataType.values()}"
                              optionKey="name"
                              optionValue="name"/></td>
            </tr>
            <tr>
                <td><label>Default Value</label></td>
                <td><input type="text" name="defaultValue" value="${field?.defaultValue}"/></td>
            </tr>

            <tr>
                <td><label>Place Holder</label></td>
                <td><input type="text" name="placeholder" value="${field?.placeHolder}"/></td>
            </tr>

            <tr>
                <td><label>Validation</label></td>
                <td><input type="text" name="validationClass" value="${field?.validationClass}"/></td>
            </tr>

            <tr>
                <td><label>Select validations</label></td>
                <td><g:select multiple="multiple" name="dataType" value="" from="${params.validations}"
                              optionKey="id"
                              optionValue="name"/></td>
            </tr>


            %{--<fieldset class="form">--}%
            %{--<f:all bean="formFields"/>--}%
            %{--</fieldset>--}%
            <tr>
                <td>
                    <fieldset class="buttons">
                        <g:submitButton name="create" class="save"
                                        value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                    </fieldset>
                </td>
            </tr>
        </table>
    </g:form>
</div>
</body>
</html>

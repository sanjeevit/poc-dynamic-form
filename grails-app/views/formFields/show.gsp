<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'formFields.name', default: 'FormFields')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-formFields" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-formFields" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <table>
                <tr>
                    <td><label>Name</label></td>
                    <td><input type="text" readonly name="name" value="${field?.name}"/></td>
                </tr>
                <tr>
                    <td><label>Name</label></td>
                    <td><input type="text" readonly name="name" value="${field?.name}"/></td>
                </tr>
                <tr>
                    <td><label>Label</label></td>
                    <td><input type="text" readonly name="label" value="${field?.label}"/></td>
                </tr>
                <tr>
                    <td><label>Data Type</label></td>
                    <td><g:select disabled="true" name="dataType" value="${field?.dataType}" from="${enums.DataType.values()}"
                                  optionKey="name"
                                  optionValue="name"/></td>
                </tr>
                <tr>
                    <td><label>Default Value</label></td>
                    <td><input type="text" readonly name="defaultValue" value="${field?.defaultValue}"/></td>
                </tr>

                <tr>
                    <td><label>Place Holder</label></td>
                    <td><input type="text" readonly name="placeholder" value="${field?.placeholder}"/></td>
                </tr>

                <tr>
                    <td><label>Validation</label></td>
                    <td><input type="text" readonly name="validationClass" value="${field?.validationClass}"/></td>
                </tr>

                <tr>
                    <td><label>Validations</label></td>
                    <td><g:select multiple="multiple" name="dataType" value="" from="${field.validations}"
                                  optionKey="id"
                                  optionValue="name"/></td>
                </tr>
            </table>

            <g:form resource="${this.formFields}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.formFields}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>

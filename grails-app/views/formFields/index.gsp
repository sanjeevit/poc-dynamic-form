<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'formFields.name', default: 'FormFields')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-formFields" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="list-formFields" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

          %{--  <f:table collection="${formFieldsList}" />--}%


            <table>

                <tr>
                    <th>Id</th>
                    <th>Label</th>
                    <th>Name</th>
                    <th>Data Type</th>

                </tr>
                <g:if test="${formFieldsList}">
                    <g:each in="${formFieldsList}" var="field">
                        <tr>
                            <td><g:link controller="formFields" action="show"
                                        params="${[id: field.id]}">${field.id}</g:link></td>
                            <td>${field.label}</td>
                            <td>${field.name}</td>
                            <td>${field.dataType}</td>

                        </tr>
                    </g:each>

                </g:if>
                <g:else>
                    <tr>
                        <td>No Result</td>
                    </tr>
                </g:else>
            </table>

            <div class="pagination">
                <g:paginate total="${formFieldsCount ?: 0}" />
            </div>
        </div>
    </body>
</html>
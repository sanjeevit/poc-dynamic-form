<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'validation.label', default: 'Validation')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-validation" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                 default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-validation" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <table>

        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Validation Type</th>
            <th>Value</th>
        </tr>
        <g:if test="${validationList}">
            <g:each in="${validationList}" var="validation">
                <tr>
                    <td><g:link controller="validation" action="show"
                                params="${[id: validation.id]}">${validation.id}</g:link></td>
                    <td>${validation.name}</td>
                    <td>${validation.validationType}</td>
                    <td>${validation.value}</td>
                </tr>
            </g:each>

        </g:if>
        <g:else>
            <tr>
                <td>No Result</td>
            </tr>
        </g:else>
    </table>
    %{--<f:table collection="${validationList}"/>--}%

    <div class="pagination">
        <g:paginate total="${validationCount ?: 0}"/>
    </div>
</div>
</body>
</html>
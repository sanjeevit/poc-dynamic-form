package poc.dynamic.form

import enums.DataType

class FormFields {
    String label
    String name
    DataType dataType
    String defaultValue
    String placeholder
    String validationClass
    List<String> radioText = ["Male", "Female"]
    List<Validation> validations

    static hasMany = [validations: Validation]

    static constraints = {
        validations nullable: true
        radioText nullable: true
        validationClass nullable: true
        validationClass nullable: true
        placeholder nullable: true
        defaultValue nullable: true
        dataType nullable: true
        name nullable: true
        label nullable: true
    }
}

package poc.dynamic.form

import enums.ValidationType

class Validation {
    String name
    ValidationType validationType
    String value

    static constraints = {
        value nullable: true
    }
}

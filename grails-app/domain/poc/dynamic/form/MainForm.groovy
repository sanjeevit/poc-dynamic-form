package poc.dynamic.form

class MainForm {
    String title
    List<FormFields> fields=[]

    static hasMany = [fields: FormFields]

    static constraints = {
    }
}

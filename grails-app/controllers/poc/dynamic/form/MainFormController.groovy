package poc.dynamic.form

import enums.poc.FormFieldDTO

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = false)
class MainFormController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<MainForm> list = MainForm.list()
        respond MainForm.list(params), model: [list: list, mainFormCount: MainForm.count()]
    }

    def addFields(Long id) {
        println "id=" + id
        MainForm mainForm = MainForm.get(id)
        List<FormFields> list = FormFields.list()
        List<FormFields> selectedList = MainForm.get(id).fields ?: []
        println "list.size" + list.size()
        list.removeAll(selectedList)
        render view: "addField", model: [title: mainForm.title, selectedList: selectedList, list: list, id: id]
    }

    def saveFormFields(FormFieldDTO formFieldDTO) {
        MainForm mainForm = MainForm.get(formFieldDTO.formId)
        List list = FormFields.createCriteria().list() {
            inList("id", formFieldDTO.fieldIds)
        } as List<FormFields>

        if (mainForm.fields) {
            mainForm.fields.addAll(list)
        } else {
            mainForm.fields = list ?: []
        }
        mainForm.fields.unique()
        mainForm.save(failOnError: true, flush: true)
        redirect(controller: "mainForm", action: "addFields", params: [id: formFieldDTO.formId])
    }

    def removeField() {
        Long formId = Long.parseLong(params.formId)
        Long fieldId = Long.parseLong(params.fieldId)
        MainForm mainForm = MainForm.get(formId)
        mainForm.fields.remove(FormFields.get(fieldId))
        mainForm.save(failOnError: true, flush: true)
        redirect(controller: "mainForm", action: "addFields", params: [id: formId])
    }

    def preview(Long id) {
        MainForm mainForm = MainForm.get(id)
        println mainForm.title
        render view: "preview", model: [mainForm: mainForm]
    }

    def show(MainForm mainForm) {
        respond mainForm
    }

    def create() {
        respond new MainForm(params)
    }

    @Transactional
    def save(MainForm mainForm) {
        if (mainForm == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (mainForm.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond mainForm.errors, view: 'create'
            return
        }

        mainForm.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'mainForm.name', default: 'MainForm'), mainForm.id])
                redirect mainForm
            }
            '*' { respond mainForm, [status: CREATED] }
        }
    }

    def edit(MainForm mainForm) {
        respond mainForm
    }

    @Transactional
    def update(MainForm mainForm) {
        if (mainForm == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (mainForm.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond mainForm.errors, view: 'edit'
            return
        }

        mainForm.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'mainForm.name', default: 'MainForm'), mainForm.id])
                redirect mainForm
            }
            '*' { respond mainForm, [status: OK] }
        }
    }

    @Transactional
    def delete(MainForm mainForm) {

        if (mainForm == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        mainForm.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'mainForm.name', default: 'MainForm'), mainForm.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'mainForm.name', default: 'MainForm'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    def submitForm(){
        render "form submited..."
    }
}

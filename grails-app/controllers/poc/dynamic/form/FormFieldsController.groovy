package poc.dynamic.form

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

//@Transactional(readOnly = false)
class FormFieldsController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond FormFields.list(), model: [formFieldsCount: FormFields.count()]
    }

    def show(Long id) {
        FormFields formFields = FormFields.get(id)
        println formFields.validations.size()
        respond formFields, model: [field: formFields]
    }

    def create() {
        List<Validation> validations = Validation.list()
        println "validations"+validations.size()
        params.validations=validations
        respond model: [field: new FormFields(), validations: validations]
    }

    def save(FormFields formFields) {
        println "hi..........."
        println formFields.errors
        if (formFields == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (formFields.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond formFields.errors, view: 'create'
            return
        }

        formFields.save flush: true,failOnError:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'formFields.name', default: 'FormFields'), formFields.id])
                redirect formFields
            }
            '*' { respond formFields, [status: CREATED] }
        }
    }

    def edit(FormFields formFields) {
        respond formFields
    }

    @Transactional
    def update(FormFields formFields) {
        println params.validations
        if (formFields == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (formFields.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond formFields.errors, view: 'edit'
            return
        }

        formFields.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'formFields.name', default: 'FormFields'), formFields.id])
                redirect formFields
            }
            '*' { respond formFields, [status: OK] }
        }
    }

    @Transactional
    def delete(FormFields formFields) {

        if (formFields == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        formFields.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'formFields.name', default: 'FormFields'), formFields.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'formFields.name', default: 'FormFields'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}

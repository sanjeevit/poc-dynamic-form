package poc.dynamic.form

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

class ValidationController {

    ValidationService validationService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Validation.list(params), model: [validationCount: Validation.count()]
    }

    def show(Validation validation) {
        respond validation
    }

    def create() {
        respond new Validation(params)
    }

    @Transactional
    def save(Validation validation) {
        if (validation == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (validation.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond validation.errors, view: 'create'
            return
        }

        validation.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'validation.label', default: 'Validation'), validation.id])
                redirect validation
            }
            '*' { respond validation, [status: CREATED] }
        }
    }

    def edit(Validation validation) {
        respond validation
    }

    @Transactional
    def update(Validation validation) {
        if (validation == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (validation.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond validation.errors, view: 'edit'
            return
        }

        validation.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'validation.label', default: 'Validation'), validation.id])
                redirect validation
            }
            '*' { respond validation, [status: OK] }
        }
    }

    @Transactional
    def delete(Validation validation) {

        if (validation == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        validation.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'validation.label', default: 'Validation'), validation.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'validation.label', default: 'Validation'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    def validate() {
        boolean status = false
        Long id = Long.parseLong(params.id)
        String value = params.value
        FormFields formFields = FormFields.get(id)
        List<Validation> validations = formFields.validations

        if(validations.size()==0){
            status=true
        }
        println "validations="+validations.size()
        for (Validation validation : validations) {
            status = validationService.verifyValidation(value, validation)
            if (!status)
                break
        }

        println "status=" + status
        println "fields id=" + id
        println "fields value=" + value
        params.message = status ? "ok" : "failed"
        respond view: "validate"
    }
}
